﻿namespace WinFormDoubleComboGrid
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvDouble = new System.Windows.Forms.DataGridView();
            this.DName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.DValue = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDouble)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvDouble
            // 
            this.dgvDouble.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDouble.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DName,
            this.DValue});
            this.dgvDouble.Location = new System.Drawing.Point(31, 60);
            this.dgvDouble.Name = "dgvDouble";
            this.dgvDouble.Size = new System.Drawing.Size(541, 210);
            this.dgvDouble.TabIndex = 0;
            // 
            // Name
            // 
            this.DName.HeaderText = "DName";
            this.DName.Name = "Name";
            this.DName.Width = 200;
            // 
            // Value
            // 
            this.DValue.HeaderText = "DValue";
            this.DValue.Name = "Value";
            this.DValue.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DValue.Width = 200;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Button1";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 412);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgvDouble);
            //this.DName = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dgvDouble)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDouble;
        private System.Windows.Forms.DataGridViewComboBoxColumn DName;
        private System.Windows.Forms.DataGridViewComboBoxColumn DValue;
        private System.Windows.Forms.Button button1;
    }
}

