﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormDoubleComboGrid
{
    public partial class Form1 : Form
    {
        private List<string> DNameLookUpLst { get; set; }
        private List<string> DValueAList { get; set; }
        private List<string> DValueBList { get; set; }
        private List<string> DValueCList { get; set; }
        private List<string> DValueDList { get; set; }

        public List<Sample> sampleLst { get; set; }

        private List<string> TLst1 = new List<string>();

        /// <summary>
        /// Constructor
        /// Used to fill Dummy data and bind grid
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            LoadDummyData();

            //BindDataGrid1();

            BindDataGrid2();
            dgvDouble.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(dgvDouble_EditingControlShowing);
            dgvDouble.CellEndEdit +=  new DataGridViewCellEventHandler(dgvDouble_CellEndEdit);
        }

        /// <summary>
        /// Method to fill Dummy data in list
        /// </summary>
        private void LoadDummyData()
        {
            DNameLookUpLst = new List<string>();
            DNameLookUpLst.Add("");
            DNameLookUpLst.Add("A");
            DNameLookUpLst.Add("B");
            DNameLookUpLst.Add("C");
            DNameLookUpLst.Add("D");

            DValueAList = new List<string>();
            DValueAList.Add("A1");
            DValueAList.Add("A2");
            DValueAList.Add("A3");

            DValueBList = new List<string>();
            DValueBList.Add("B1");
            DValueBList.Add("B2");
            DValueBList.Add("B3");
            DValueBList.Add("B4");
            DValueBList.Add("B5");

            DValueCList = new List<string>();
            DValueCList.Add("C1");
            DValueCList.Add("C2");

            DValueDList = new List<string>();
            DValueDList.Add("D1");
            DValueDList.Add("D2");
            DValueDList.Add("D3");
            DValueDList.Add("D4");
            DValueDList.Add("D5");
            DValueDList.Add("D6");
            DValueDList.Add("D7");
            DValueDList.Add("D8");

            sampleLst = new List<Sample>();
            sampleLst.Add(new Sample { Name = "C", Value = "C2" });
            sampleLst.Add(new Sample { Name = "B", Value = "B5" });
            sampleLst.Add(new Sample { Name = "A", Value = "A3" });
            sampleLst.Add(new Sample { Name = "D", Value = "D7" });
            sampleLst.Add(new Sample { Name = "C", Value = "C1" });
        }

        #region Unused1
        /*
        private void BindDataGrid1()
        {
            dgvDouble.Rows.Add(new DataGridViewRow());
            dgvDouble.Rows.Add(new DataGridViewRow());
            dgvDouble.Rows.Add(new DataGridViewRow());
            dgvDouble.Rows.Add(new DataGridViewRow());
            dgvDouble.Rows.Add(new DataGridViewRow());

            DataGridViewRow dr1 = dgvDouble.Rows[0];
            DataGridViewComboBoxCell dc11 = (DataGridViewComboBoxCell)dr1.Cells[0];
            dc11.DataSource = DNameLookUpLst;
            dc11.Value = sampleLst[0].Name;
            DataGridViewComboBoxCell dc12 = (DataGridViewComboBoxCell)dr1.Cells[1];
            dc12.DataSource = DValueCList;
            dc12.Value = sampleLst[0].Value;

            DataGridViewRow dr2 = dgvDouble.Rows[1];
            DataGridViewComboBoxCell dc21 = (DataGridViewComboBoxCell)dr2.Cells[0];
            dc21.DataSource = DNameLookUpLst;
            dc21.Value = sampleLst[1].Name;
            DataGridViewComboBoxCell dc22 = (DataGridViewComboBoxCell)dr2.Cells[1];
            dc22.DataSource = DValueBList;
            dc22.Value = sampleLst[1].Value;

            DataGridViewRow dr3 = dgvDouble.Rows[2];
            DataGridViewComboBoxCell dc31 = (DataGridViewComboBoxCell)dr3.Cells[0];
            dc31.DataSource = DNameLookUpLst;
            dc31.Value = sampleLst[2].Name;
            DataGridViewComboBoxCell dc32 = (DataGridViewComboBoxCell)dr3.Cells[1];
            dc32.DataSource = DValueAList;
            dc32.Value = sampleLst[2].Value;

            DataGridViewRow dr4 = dgvDouble.Rows[3];
            DataGridViewComboBoxCell dc41 = (DataGridViewComboBoxCell)dr4.Cells[0];
            dc41.DataSource = DNameLookUpLst;
            dc41.Value = sampleLst[3].Name;
            DataGridViewComboBoxCell dc42 = (DataGridViewComboBoxCell)dr4.Cells[1];
            dc42.DataSource = DValueDList;
            dc42.Value = sampleLst[3].Value;

            DataGridViewRow dr5 = dgvDouble.Rows[4];
            DataGridViewComboBoxCell dc51 = (DataGridViewComboBoxCell)dr5.Cells[0];
            dc51.DataSource = DNameLookUpLst;
            dc51.Value = sampleLst[4].Name;
            DataGridViewComboBoxCell dc52 = (DataGridViewComboBoxCell)dr5.Cells[1];
            dc52.DataSource = DValueCList;
            dc52.Value = sampleLst[4].Value;
        }
         * */
        #endregion

        /// <summary>
        /// Method to handle control editing event  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvDouble_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //e.CellStyle.BackColor = Color.Aquamarine;
            if (e.Control != null)
            {
                int colIndex = dgvDouble.CurrentCell.ColumnIndex;
                if (e.Control is ComboBox && colIndex == 0)
                {
                    ComboBox editingCombo = (ComboBox)e.Control;

                    editingCombo.SelectedIndexChanged -= new EventHandler(editingCombo_SelectedIndexChanged);
                    editingCombo.SelectedIndexChanged += new EventHandler(editingCombo_SelectedIndexChanged);

                    //editingCombo.SelectionChangeCommitted -= new EventHandler(editingCombo_SelectionChangeCommitted);
                    //editingCombo.SelectionChangeCommitted += new EventHandler(editingCombo_SelectionChangeCommitted);

                    //editingCombo.TextChanged -= new EventHandler(editingCombo_TextChanged);
                    //editingCombo.TextChanged += new EventHandler(editingCombo_TextChanged);

                    //editingCombo.DropDownClosed -= new EventHandler(editingCombo_DropDownClosed);
                    //editingCombo.DropDownClosed += new EventHandler(editingCombo_DropDownClosed);
                }
            }
        }

        private void dgvDouble_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.Control != null)
            //{
            //    int colIndex = dgvDouble.CurrentCell.ColumnIndex;
            //    if (e.Control is ComboBox && colIndex == 0)
            //    {
            //        ComboBox editingCombo = (ComboBox)e.Control;
            //    }
            //}

            object val = dgvDouble.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;

        }
        /// <summary>
        /// Method to handle comboBox selected Index changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editingCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    int colIndex = dgvDouble.CurrentCell.ColumnIndex;

                    if (sender is ComboBox && colIndex == 0)
                    {
                        ComboBox cb = (ComboBox)sender;
                        if (cb != null)
                        {
                            if (cb.SelectedItem != null)
                            {
                                string selectedItem = cb.SelectedItem.ToString();

                                DataGridViewRow dr = dgvDouble.CurrentRow;
                                DataGridViewComboBoxCell dc = (DataGridViewComboBoxCell)dr.Cells[1];
                                //dc.Value = "";
                                // Imp: set the value to null ( so as to avoid exception when DataSource is changed)
                                dc.Value = null;
                                //dc.DataSource = null;
                                TLst1[dr.Index] = selectedItem;

                                switch (selectedItem)
                                {
                                    case "A":
                                        dc.DataSource = DValueAList;
                                        break;
                                    case "B":
                                        dc.DataSource = DValueBList;
                                        break;
                                    case "C":
                                        dc.DataSource = DValueCList;
                                        break;
                                    case "D":
                                        dc.DataSource = DValueDList;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void editingCombo_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    int colIndex = dgvDouble.CurrentCell.ColumnIndex;

                    if (sender is ComboBox && colIndex == 0)
                    {
                        ComboBox cb = (ComboBox)sender;
                        if (cb != null)
                        {
                            if (cb.SelectedItem != null)
                            {
                                string selectedItem = cb.SelectedItem.ToString();

                                DataGridViewRow dr = dgvDouble.CurrentRow;
                                DataGridViewComboBoxCell dc = (DataGridViewComboBoxCell)dr.Cells[1];
                                //dc.Value = "";
                                // Imp: set the value to null ( so as to avoid exception when DataSource is changed)
                                dc.Value = null;
                                //dc.DataSource = null;
                                TLst1[dr.Index] = selectedItem;

                                switch (selectedItem)
                                {
                                    case "A":
                                        dc.DataSource = DValueAList;
                                        break;
                                    case "B":
                                        dc.DataSource = DValueBList;
                                        break;
                                    case "C":
                                        dc.DataSource = DValueCList;
                                        break;
                                    case "D":
                                        dc.DataSource = DValueDList;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }


        private void editingCombo_TextChanged(object sender, EventArgs e)
        {
        }

        private void editingCombo_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    int colIndex = dgvDouble.CurrentCell.ColumnIndex;

                    if (sender is ComboBox && colIndex == 0)
                    {
                        ComboBox cb = (ComboBox)sender;
                        if (cb != null)
                        {
                            if (cb.SelectedItem != null)
                            {
                                string selectedItem = cb.SelectedItem.ToString();

                                DataGridViewRow dr = dgvDouble.CurrentRow;
                                DataGridViewComboBoxCell dc = (DataGridViewComboBoxCell)dr.Cells[1];
                                //dc.Value = "";
                                // Imp: set the value to null ( so as to avoid exception when DataSource is changed)
                                dc.Value = null;
                                //dc.DataSource = null;
                                TLst1[dr.Index] = selectedItem;

                                switch (selectedItem)
                                {
                                    case "A":
                                        dc.DataSource = DValueAList;
                                        break;
                                    case "B":
                                        dc.DataSource = DValueBList;
                                        break;
                                    case "C":
                                        dc.DataSource = DValueCList;
                                        break;
                                    case "D":
                                        dc.DataSource = DValueDList;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Method to populate DataGridView
        /// </summary>
        private void BindDataGrid2()
        {
            DName.DataSource = DNameLookUpLst;
            TLst1 = new List<string>();

            dgvDouble.Rows.Add(new DataGridViewRow());
            dgvDouble.Rows.Add(new DataGridViewRow());
            dgvDouble.Rows.Add(new DataGridViewRow());
            dgvDouble.Rows.Add(new DataGridViewRow());
            dgvDouble.Rows.Add(new DataGridViewRow());

            TLst1.Add("");
            TLst1.Add("");
            TLst1.Add("");
            TLst1.Add("");
            TLst1.Add("");

            DataGridViewRow dr1 = dgvDouble.Rows[0];
            DataGridViewComboBoxCell dc11 = (DataGridViewComboBoxCell)dr1.Cells[0];
            //dc11.Value = sampleLst[0].Name;
            dc11.Value = "";

            DataGridViewComboBoxCell dc12 = (DataGridViewComboBoxCell)dr1.Cells[1];
            dc12.DataSource = DValueCList;
            //dc12.Value = sampleLst[0].Value;

            DataGridViewRow dr2 = dgvDouble.Rows[1];
            DataGridViewComboBoxCell dc21 = (DataGridViewComboBoxCell)dr2.Cells[0];
            //dc21.Value = sampleLst[1].Name;
            DataGridViewComboBoxCell dc22 = (DataGridViewComboBoxCell)dr2.Cells[1];
            dc22.DataSource = DValueBList;
            //dc22.Value = sampleLst[1].Value;

            DataGridViewRow dr3 = dgvDouble.Rows[2];
            DataGridViewComboBoxCell dc31 = (DataGridViewComboBoxCell)dr3.Cells[0];
            dc31.Value = sampleLst[2].Name;
            DataGridViewComboBoxCell dc32 = (DataGridViewComboBoxCell)dr3.Cells[1];
            dc32.DataSource = DValueAList;
            dc32.Value = sampleLst[2].Value;

            DataGridViewRow dr4 = dgvDouble.Rows[3];
            DataGridViewComboBoxCell dc41 = (DataGridViewComboBoxCell)dr4.Cells[0];
            dc41.Value = sampleLst[3].Name;
            DataGridViewComboBoxCell dc42 = (DataGridViewComboBoxCell)dr4.Cells[1];
            dc42.DataSource = DValueDList;
            dc42.Value = sampleLst[3].Value;

            DataGridViewRow dr5 = dgvDouble.Rows[4];
            DataGridViewComboBoxCell dc51 = (DataGridViewComboBoxCell)dr5.Cells[0];
            dc51.Value = sampleLst[4].Name;
            DataGridViewComboBoxCell dc52 = (DataGridViewComboBoxCell)dr5.Cells[1];
            dc52.DataSource = DValueCList;
            dc52.Value = sampleLst[4].Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TLst1[0]))
            {
                RetryFetchingValues();
            }
        }

        private void RetryFetchingValues()
        {
            TLst1[0] = dgvDouble.Rows[0].Cells[0].Value.ToString();
        }

        
    }

    /// <summary>
    /// Sample class to bind grid
    /// </summary>
    public class Sample
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
